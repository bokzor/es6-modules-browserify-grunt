'use strict';

module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    // Concatenates CSS and js files into a single bundle of CSS and js files. In this example I configured it to concatenate CSS files only.
    concat: {
      css:{
        src: ['src/modules/**/*.css'],
        dest: 'dev/app.css'
      }
    },
    // Run a local node.js server and serve static files.
    connect: {
      server: {
        options: {
          port: 3000,
          base: 'dev'
        }
      }
    },
    // Watches for changes in files and runs certain Grunt tasks.
    watch: {
      js: {
        files: ['src/**/*.js','src/*.js'],
        tasks: ['browserify']
      },
      css: {
        files: ['<%= concat.css.src %>'],
        tasks: ['concat:css']
      },
      html: {
        files: ['src/*.html','src/**/*.html'],
        tasks: ['copy']
      }
    },
    // Compiles ES6 modules so we can use it in the browser.
    // I’m using its transform option so it will also transpile the code from ES6 to JavaScript with Babel.
    browserify: {
      dist: {
        files: {
          'dev/app.js': ['src/app.js'],
        },
        options: {
          transform: ['babelify'],
          browserifyOptions: {
             debug: true
          }
        }
      }
    },
    // Copies files from one directory to another.
    // In our case it copies all HTML templates to dev/templates and index.html to dev directories.
    copy: {
      html: {
        files : [
          {
            expand : true,
            dest   : 'dev/templates',
            cwd    : 'src/modules',
            flatten: true,
            src    : [
              '**/*.html'
            ]
          },
          {
            expand : true,
            dest   : 'dev',
            cwd    : 'src',
            src    : [
              'index.html'
            ]
          }
        ]
      }
    }
    
  });

  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-browserify');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-contrib-copy');

  grunt.registerTask('default', ['connect','browserify', 'concat', 'copy', 'watch']);

};
