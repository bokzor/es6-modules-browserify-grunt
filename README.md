#ECMAScript 6, Babel, Browserify with Grunt#

This is a very simple Angular web app with only two views and two modules.
The purpose is to create a development workflow with ES6 and its modules using Browserify.
The Gruntfile.js configured for development stage only.
To use it in production you should modify this file and add few more tasks like jsLint and uglify.

### How do I get set up? ###

* clone repository: git clone git@bitbucket.org:appsflyer/es6-modules-browserify-grunt.git
* sudo npm install
* grunt
* go to http://localhost:3000/index.html

### How modules defined? ###
I’ve created two modules: first.module and second.module.
Each module consists of other modules: angular service and angular controller.
Services and controllers are pure ES6 code which can be reused whenever we want.

![diagram.png](https://bitbucket.org/appsflyer/es6-modules-browserify-grunt/downloads/tree.png)
