import angular from 'angular'
import firstmodule from './modules/first.module/first.module'
import secondmodule from './modules/second.module/second.module'

angular.module('app', [
		'app.firstmodule',
		'app.secondmodule'
]);

angular.element(document)
	.ready(() => angular.bootstrap(document, ['app']),{strictDi:true});